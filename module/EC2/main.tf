#to create AWS Instance
resource "aws_instance" "name1" {
  ami = var.ami
  instance_type = var.instance_type
  key_name = var.key
  tags = merge({
    Name = format("DevOps-b23-%s-%s", var.name, var.env, )
  }, var.tags)
}

output "instance_ip_addr" {
  value = aws_instance.name1.public_ip
}