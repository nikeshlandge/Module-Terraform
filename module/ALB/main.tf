# Create Application load balancer.
resource "aws_lb" "ALB" {
  name                       = "ALB"
  internal                   = false
  load_balancer_type         = "application"
#   security_groups            = [sg-044e8dfa8c33b1673]
  subnets            = [for subnet in aws_subnet.public : subnet.id]
#   subnets                    = [subnet-0807585e917d562b9, subnet-06309c9150dcf7e02]
  enable_deletion_protection = false
  tags = {
    Env = "test"
  }
}

# #create lb listener 
resource "aws_lb_listener" "front_end" {
  load_balancer_arn = aws_lb.ALB.arn
  port              = "80"
  protocol          = "HTTP"
  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.TG-1.arn
  }
}

# create Load balancer target group.
resource "aws_lb_target_group" "TG-1" {
  name     = "TG-1"
  port     = 80
  protocol = "HTTP"
  vpc_id   = "vpc-005d54fab7f976dc3"
  tags = {
    Env = "test"
  }
}


#create lb target group attachment.
# resource "aws_lb_target_group_attachment" "test" {
#   target_group_arn = aws_lb_target_group.TG-1.arn
#   target_id        = aws_instance.TG-1.id
#   port             = 80
# }
