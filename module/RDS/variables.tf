variable "allocated_storage" {
    type = number
}
variable "max_allocated_storage" {
    type = number
}

# variable "az1" {
#     type = string
# }

variable "db_name" {
  type = string
}
variable "engine" {
  type = string
}
variable "engine_version" {
  type    = string
}

variable "instance_class" {
  type    = string
}

variable "skip_final_snapshot" {
  type    = bool
}

variable "username" {
  type    = string
}

variable "password" {
  type    = string
}

variable "parameter_group_name" {
  type    = string
}

variable "tags" {
  type = map(string)
  default = {}
}