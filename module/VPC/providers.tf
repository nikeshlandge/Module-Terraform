terraform {
  required_providers {
    aws = {
      source = "hashicorp/aws"
      version = "~> 4.0"
      # profile = "Nikesh"
      # region  = "us-east-1"
    }
  }
}

provider "aws" {
  # Configuration options
  profile = "Nikesh"
  region  = "us-east-1"
}

#terraform-aws-remote-state-s3-backend
# create s3 bucket firstly for remote state backend.
# terraform {
#   backend "s3" {
#     bucket         = "devops-b23-bucket"
#     key            = "Dev/terraform.tfstate"
#     region         = "us-east-1"
#     profile        = "Nikesh"
#     dynamodb_table = "terraform-state-lock-dynamo"
#   }
# } 