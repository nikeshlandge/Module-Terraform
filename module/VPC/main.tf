# Create a VPC
resource "aws_vpc" "my_vpc" {
  cidr_block = var.vpc_cidr
  tags = {
    Name = "${var.name}-my_vpc_nik"
    Env = var.env
  }
}

resource "aws_subnet" "public_subnet_a" {
  vpc_id            = aws_vpc.my_vpc.id
  cidr_block        = var.public_subnet_a_cidr
  map_public_ip_on_launch = true
  availability_zone = var.az1
  tags = {
    Name = "${var.name}-public-subnet_a"
    Env = var.env
  }
}

resource "aws_subnet" "public_subnet_b" {
  vpc_id            = aws_vpc.my_vpc.id
  cidr_block        = var.public_subnet_b_cidr
  map_public_ip_on_launch = false
  availability_zone = var.az2
  tags = {
    Name = "${var.name}-public-subnet_b"
    Env = var.env
  }
}
#create IGW for VPC.
resource "aws_internet_gateway" "igw" {
  vpc_id = aws_vpc.my_vpc.id

  tags = {
    Name = "${var.name}-main_igw"
    Env = var.env
  }
}

#Create Default route table.
resource "aws_default_route_table" "example" {
  default_route_table_id = aws_vpc.my_vpc.default_route_table_id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.igw.id
  }
  tags = {
    Name = "Default-Rout"
    Env = var.env
  }
}