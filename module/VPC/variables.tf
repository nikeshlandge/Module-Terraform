variable "ami_id" {
}
#  type = string
#  description = "ENTER_AMI_ID"
# }
variable "vpc_cidr"{
}
variable "name"{
}
variable "az1"{
 }
variable "az2"{
 }
variable "private_subnet_a_cidr"{
 }
variable "private_subnet_b_cidr"{
 }
 variable "public_subnet_a_cidr"{
 }
 variable "public_subnet_b_cidr"{
 }
 variable "env"{
 }
 variable "instance_type" {
}
variable "ssh_key" {
}
variable "max_size" { 
}
variable "min_size" {
}
variable "desired_capacity" {
}