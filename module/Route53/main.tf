resource "aws_s3_bucket" "b" {
  bucket = var.bucket_name
  acl    = var.bucket_acl
  policy = var.bucket_policy

  website {
    index_document = "index.html"
    error_document = "index.html"
  }
  force_destroy = true
}

resource "aws_s3_bucket_website_configuration" "b" {
  bucket = aws_s3_bucket.b.bucket

  index_document {
    suffix = "index.html"
  }
}
resource "aws_s3_bucket_object" "b" {
  key    = var.bucket_object_key
  bucket = aws_s3_bucket.b.id
  source = var.bucket_object_source

}

resource "aws_route53_zone" "this" {
  name = var.bucket_name
}

resource "aws_route53_record" "www" {
  zone_id = aws_route53_zone.this.id
  name    = var.bucket_name
  type    = var.record_type

  alias {
    name = aws_s3_bucket.b.website_endpoint
    zone_id = aws_s3_bucket.b.hosted_zone_id
    evaluate_target_health = true
  }
}

