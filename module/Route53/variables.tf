variable "bucket_name" {
    type = string
}

variable "bucket_acl" {
  type = string
}
variable "bucket_policy" {
  type = any
}
variable "bucket_object_key" {
  type    = string
}

variable "bucket_object_source" {
  type    = string
}
variable "record_type" {
  type    = string
}

# variable "tags" {
#   type = map(string)
#   default = {}
# }