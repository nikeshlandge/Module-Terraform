variable "name" {
  type = string
}

variable "tags" {
  type = map(string)
  default = {}
}

variable "acl" {
  type = string
}
variable "origin_id" {
  type = string
}
variable "versioning_status" {
  type = string
}
variable "Allowed_HTTP_methods" {
  type = list
}
variable "cached_methods" {
  type = list
}
variable "viewer_protocol_policy" {
  type = string
}
variable "restriction_type" {
  type = string
}
variable "locations" {
  type = list
}