terraform {
  backend "s3" {
    bucket         = "devops-b23-bucket"
    key            = "Dev/terraform.tfstate"
    region         = "us-east-1"
    profile        = "Nikesh"
    dynamodb_table = "terraform-state-lock-dynamo"
  }
}

provider "aws" {
  # Configuration options
  profile = "Nikesh"
  region  = "us-east-1"
} 