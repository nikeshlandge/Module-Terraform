  #--------------RDS-----------------------#
  allocated_storage     = 10
  max_allocated_storage = 100
  db_name               = "mydb"
  engine                = "mysql"
  engine_version        = "5.7"
  instance_class        = "db.t3.micro"
  username              = "admin"
  password              = "redhat123"
  parameter_group_name  = "default.mysql5.7"
  skip_final_snapshot   = true
  tags = {
    env       = "development"
    bill_unit = "zshapr-102"
    owner     = "ecom"
    mail      = "nikeshlandge@outlook.com"
    team      = "DevOps"
  }
    #--------------RDS-----------------------#

  #--------------Route53-----------------------#
  bucket_name = "hellomotoshubham.tk"
bucket_acl = "public-read"
bucket_policy = <<EOF
{
  "Version":"2012-10-17",
  "Statement":[{
        "Sid":"PublicReadForGetBucketObjects",
        "Effect":"Allow",
          "Principal": "*",
      "Action":["s3:GetObject"],
      "Resource":["arn:aws:s3:::hellomotoshubham.tk/*"]
    }
  ]
}
EOF
bucket_object_key = "index.html"
bucket_object_source = "../module/Route53/index.html"
record_type = "A"
  #--------------Route53-----------------------#