# module "EC2_instance" {
#   source        = "../EC2/"
#   env           = "dev"
#   name          = "nikeshrao"
#   ami           = "ami-05fa00d4c63e32376"
#   key           = "joge-vg"
#   instance_type = "t2.micro"
#   tags = {
#     env       = "development"
#     bill_unit = "zshapr-102"
#     owner     = "ecom"
#     mail      = "nikeshlandge@outlook.com"
#     team      = "DevOps"
#   }
# }

# module "s3_bucket" {
#   source = "../S3/"
#   env    = "Test"
#   name   = "purasad-bucket"
#   tags = {
#     env       = "development"
#     bill_unit = "zshapr-102"
#     owner     = "ecom"
#     mail      = "nikeshlandge@outlook.com"
#     team      = "DevOps"
#   }
# }

# module "cdn" {
#   source = "../Cloudfront"
#   for_each = toset(var.bucket_name)
#   name   = each.key
#   tags = {
#     env       = "development"
#     bill_unit = "zshapr-102"
#     owner     = "ecom"
#     mail      = "nikeshlandge@outlook.com"
#     team      = "DevOps"
#   }
#   acl                    = "public-read"
#   origin_id              = "myS3Origin"
#   versioning_status      = "Enabled"
#   Allowed_HTTP_methods   = ["DELETE", "GET", "HEAD", "OPTIONS", "PATCH", "POST", "PUT"]
#   cached_methods         = ["GET", "HEAD"]
#   viewer_protocol_policy = "allow-all" //HTTP and HTTPS or  //Redirect HTTP to HTTPS or   //HTTPS only
#   restriction_type       = "whitelist" //"blacklist" //"none"
#   locations              = ["US", "CA", "GB", "DE"]
# } 

# module "RDS" {
#   source                = "../module/RDS/"
#   allocated_storage     = var.allocated_storage
#   max_allocated_storage = var.max_allocated_storage
#   db_name               = var.db_name
#   engine                = var.engine
#   engine_version        = var.engine_version
#   instance_class        = var.instance_class
#   username              = var.username
#   password              = var.password
#   parameter_group_name  = var.parameter_group_name
#   skip_final_snapshot   = var.skip_final_snapshot
#   tags = var.tags
# }

module "Route53" {
  source                = "../module/Route53/"
  bucket_name = var.bucket_name
  bucket_acl = var.bucket_acl
  bucket_policy = var.bucket_policy
  bucket_object_key = var.bucket_object_key
  bucket_object_source = var.bucket_object_source
  record_type = var.record_type
}