variable "allocated_storage" {
    type = number
}
variable "max_allocated_storage" {
    type = number
}

variable "db_name" {
  type = string
}
variable "engine" {
  type = string
}
variable "engine_version" {
  type    = string
}

variable "instance_class" {
  type    = string
}

variable "skip_final_snapshot" {
  type    = bool
}

variable "username" {
  type    = string
}

variable "password" {
  type    = string
}

variable "parameter_group_name" {
  type    = string
}

variable "tags" {
  type = map(string)
  default = {}
}
#--------------Route53-----------------------#
variable "bucket_name" {
    type = string
}

variable "bucket_acl" {
  type = string
}
variable "bucket_policy" {
  type = any
}
variable "bucket_object_key" {
  type    = string
}

variable "bucket_object_source" {
  type    = string
}
variable "record_type" {
  type    = string
}
#--------------Route53-----------------------#