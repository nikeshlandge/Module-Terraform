# variable "iam_username" {
#   type = list
#   default = ["nikesh1","nikesh2","nikesh3","nikesh4","nikesh5","nikesh6","nikesh7","nikesh8","nikesh9","nikesh10"]
# }

# variable "s3_bucket_names" {
#   type = list
#   default = ["nikbucki-1", "nikbucki-2", "nikbucki-3", "nikbucki-4", "nikbucki-5", "nikbucki-6", "nikbucki-7", "nikbucki-8", "nikbucki-9", "nikbucki-10", "nikbucki-11","nikbucki-12","nikbucki-13","nikbucki-14","nikbucki-15"]
# }
variable "vpc_cidr" {
}
variable "name" {
}
variable "az1" {
}
variable "az2" {
}
variable "private_subnet_a_cidr" {
}
variable "private_subnet_b_cidr" {
}
variable "private_subnet_c_cidr" {
}
variable "private_subnet_d_cidr" {
}
variable "public_subnet_a_cidr" {
}
variable "public_subnet_b_cidr" {
}