# #to create Application load balancer
# resource "aws_elb" "web_elb" {
#   name = "web-elb"
#   subnets = [aws_subnet.public_subnet-a.id, aws_subnet.public_subnet-b.id]
#   listener {
#     lb_port = 80
#     lb_protocol = "http"
#     instance_port = "80"
#     instance_protocol = "http"
#   }
# }
# output "dns_lb" {
#   description = "DNS of load balancer"
#   value = aws_elb.web_elb.dns_name
# }