
#to create AWS Instance
resource "aws_instance" "name1" {
  ami = "ami-05fa00d4c63e32376"
  instance_type = "t2.micro"
  key_name = "joge-vg"
  tags = {
    Name = var.name
  }
}
# #create IAM user
# # resource "aws_iam_user" "IAM_nik_1" {
# #   count = length(var.iam_username)
# #   name = var.iam_username[count.index]
# #   path = "/system/"
# # }

# #To Create multiple bucket at single resource

# # resource "aws_s3_bucket" "nik_s3_buckets" {
# #   count         = length(var.s3_bucket_names)
# #   bucket        = var.s3_bucket_names[count.index]
# #   force_destroy = false
# # }

# #to create VPC

# resource "aws_vpc" "my_vpc" {
#   cidr_block = "172.16.0.0/16"

#   tags = {
#     Name = var.name
#   }
# }

# #create IGW for VPC.
# resource "aws_internet_gateway" "igw" {
#   vpc_id = aws_vpc.my_vpc.id
#   tags = {
#     Name = "${var.name}-igw"
#   }
# }

# #Create NAT for VPC

# resource "aws_nat_gateway" "my_NAT" {
#   allocation_id = aws_eip.eip1.id
#   subnet_id     = aws_subnet.public_subnet-b.id

#   tags = {
#     Name = "${var.name}-NAT"
#   }
#   depends_on = [aws_internet_gateway.igw]
# }

# #Elastic IP to NAT
# resource "aws_eip" "eip1" {
#   vpc = true
# }
# output "nat_gateway_ip" {
#   value = aws_eip.eip1.public_ip
# }
# # resource "aws_eip" "eip" {
# #   vpc = true
# # }
# # resource "aws_route_table_association" "nat_gateway" {
# #   subnet_id = aws_subnet.nat_gateway.id
# #   route_table_id = aws_route_table.nat_gateway.id
# # }

# #to create subnets

# resource "aws_subnet" "public_subnet-a" {
#   vpc_id                  = aws_vpc.my_vpc.id
#   cidr_block              = var.public_subnet_a_cidr
#   map_public_ip_on_launch = true
#   availability_zone       = var.az1
#   tags = {
#     Name = "${var.name}-public_a-subnet"
#   }
# }
# resource "aws_subnet" "public_subnet-b" {
#   vpc_id                  = aws_vpc.my_vpc.id
#   cidr_block              = var.public_subnet_b_cidr
#   map_public_ip_on_launch = true
#   availability_zone       = var.az2
#   tags = {
#     Name = "${var.name}-public_b-subnet"
#   }
# }

# resource "aws_subnet" "private_subnet-1a" {
#   vpc_id                  = aws_vpc.my_vpc.id
#   cidr_block              = var.private_subnet_a_cidr
#   map_public_ip_on_launch = false
#   availability_zone       = var.az1
#   tags = {
#     Name = "${var.name}-private-subnet-a1"
#   }
# }

# resource "aws_subnet" "private_subnet-2a" {
#   vpc_id                  = aws_vpc.my_vpc.id
#   cidr_block              = var.private_subnet_b_cidr
#   map_public_ip_on_launch = false
#   availability_zone       = var.az1
#   tags = {
#     Name = "${var.name}-private-subnet"
#   }
# }
# resource "aws_subnet" "private_subnet-1b" {
#   vpc_id                  = aws_vpc.my_vpc.id
#   cidr_block              = var.private_subnet_c_cidr
#   map_public_ip_on_launch = false
#   availability_zone       = var.az2
#   tags = {
#     Name = "${var.name}-private-subnet"
#   }
# }

# resource "aws_subnet" "private_subnet-2b" {
#   vpc_id                  = aws_vpc.my_vpc.id
#   cidr_block              = var.private_subnet_d_cidr
#   map_public_ip_on_launch = false
#   availability_zone       = var.az2
#   tags = {
#     Name = "${var.name}-private-subnet"
#   }
# }

# # Create  Public route table
# resource "aws_route_table" "Public_RT" {
#   vpc_id = aws_vpc.my_vpc.id


#   route {
#     cidr_block = "0.0.0.0/0"
#     gateway_id = aws_internet_gateway.igw.id
#   }

#   tags = {
#     Name = "${var.name}-Public_RT"
#   }
# }
# # Create  Private route table
# resource "aws_route_table" "Private_RT" {
#   vpc_id = aws_vpc.my_vpc.id


#   route {
#     cidr_block = "0.0.0.0/0"
#     gateway_id = aws_internet_gateway.igw.id
#   }

#   tags = {
#     Name = "${var.name}-Private_RT"
#   }
# }

# # Create Public Subnet association with Public route table
# resource "aws_route_table_association" "a" {
#   subnet_id      = aws_subnet.public_subnet-a.id
#   route_table_id = aws_route_table.Public_RT.id
# }

# # Create Private Subnet association with Private route table
# resource "aws_route_table_association" "b1" {
#   subnet_id      = aws_subnet.private_subnet-1a.id
#   route_table_id = aws_route_table.Private_RT.id
# }
# resource "aws_route_table_association" "b2" {
#   subnet_id      = aws_subnet.private_subnet-2a.id
#   route_table_id = aws_route_table.Private_RT.id
# }
# resource "aws_route_table_association" "b3" {
#   subnet_id      = aws_subnet.private_subnet-1b.id
#   route_table_id = aws_route_table.Private_RT.id
# }
# resource "aws_route_table_association" "b4" {
#   subnet_id      = aws_subnet.private_subnet-2b.id
#   route_table_id = aws_route_table.Private_RT.id
# }

# # Create Security Group
# resource "aws_security_group" "My_SG" {
#   name        = "${var.name}-SG"
#   description = "Allow HTTP inbound traffic"
#   vpc_id      = aws_vpc.my_vpc.id

#   #Inbound rules.
#   ingress {
#     description = "SSH"
#     from_port   = 22
#     to_port     = 22
#     protocol    = "tcp"
#     cidr_blocks = ["0.0.0.0/0"]
#   }
#   ingress {
#     description = "http"
#     from_port   = 80
#     to_port     = 80
#     protocol    = "tcp"
#     cidr_blocks = ["0.0.0.0/0"]
#   }
#   #Outbound rules.
#   egress {
#     from_port   = 0
#     to_port     = 0
#     protocol    = "-1"
#     cidr_blocks = ["0.0.0.0/0"]
#   }
# }